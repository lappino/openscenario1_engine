/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_2/impl/ApiClassImplV1_2.h>
#include <openScenarioLib/src/impl/NamedReferenceProxy.h>

#include <memory>

#include "Node/AllTriggeringEntityNode.h"
#include "Node/AnyTriggeringEntityNode.h"
#include "Node/ByEntityConditionNode.h"
#include "TestUtils.h"

using testing::OpenScenarioEngine::v1_2::OpenScenarioEngineLibraryTestBase;
using units::literals::operator""_mps;

namespace detail
{
std::shared_ptr<OpenScenarioEngine::v1_2::Node::ByEntityConditionNode> CreateByEntityConditionUnderTest(const NET_ASAM_OPENSCENARIO::v1_2::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum triggering_entities_rule)
{
  auto speed_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_2::SpeedConditionImpl>();
  speed_condition->SetRule(NET_ASAM_OPENSCENARIO::v1_2::Rule::RuleEnum::LESS_THAN);
  speed_condition->SetValue(1.0);
  auto entity_condition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_2::EntityConditionImpl>();
  entity_condition->SetSpeedCondition(speed_condition);
  auto entity_ref_ptr = std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_2::IEntity>>("Ego");
  auto entity_ref = std::make_shared<NET_ASAM_OPENSCENARIO::v1_2::EntityRefImpl>();
  entity_ref->SetEntityRef(entity_ref_ptr);
  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_2::IEntityRefWriter>> entity_refs;
  entity_refs.push_back(entity_ref);
  auto triggering_entities = std::make_shared<NET_ASAM_OPENSCENARIO::v1_2::TriggeringEntitiesImpl>();
  triggering_entities->SetTriggeringEntitiesRule(triggering_entities_rule);
  triggering_entities->SetEntityRefs(entity_refs);
  auto byEntityCondition = std::make_shared<NET_ASAM_OPENSCENARIO::v1_2::ByEntityConditionImpl>();
  byEntityCondition->SetTriggeringEntities(triggering_entities);
  byEntityCondition->SetEntityCondition(entity_condition);
  return std::make_shared<OpenScenarioEngine::v1_2::Node::ByEntityConditionNode>(byEntityCondition);
}
}  // namespace detail

TEST_F(OpenScenarioEngineLibraryTestBase, GivenTriggeringEntitiesRuleAny_WhenConstructByEntityConditionNode_ThenContainsAnyTriggeringEntityNode)
{
  auto by_entity_condition_under_test = detail::CreateByEntityConditionUnderTest(NET_ASAM_OPENSCENARIO::v1_2::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ANY);

  ASSERT_TRUE(by_entity_condition_under_test->hasChild());
  ASSERT_NO_THROW(dynamic_cast<OpenScenarioEngine::v1_2::Node::AnyTriggeringEntityNode &>(by_entity_condition_under_test->child()));
}

TEST_F(OpenScenarioEngineLibraryTestBase, GivenTriggeringEntitiesRuleAll_WhenConstructByEntityConditionNode_ThenContainsAllTriggeringEntityNode)
{
  auto by_entity_condition_under_test = detail::CreateByEntityConditionUnderTest(NET_ASAM_OPENSCENARIO::v1_2::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ALL);

  ASSERT_TRUE(by_entity_condition_under_test->hasChild());
  ASSERT_NO_THROW(dynamic_cast<OpenScenarioEngine::v1_2::Node::AllTriggeringEntityNode &>(by_entity_condition_under_test->child()));
}

TEST_F(OpenScenarioEngineLibraryTestBase, GivenByEntityConditionNode_WhenTick_ThenReturnAlwaysChildStatus)
{
  EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle &>(env_->GetEntityRepository().Get("Ego").value().get()), GetVelocity())
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0_mps, 0_mps}))
      .WillOnce(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{10.0_mps, 0_mps, 0_mps}))
      .WillRepeatedly(::testing::Return(mantle_api::Vec3<units::velocity::meters_per_second_t>{0.0_mps, 0_mps, 0_mps}));
  auto engine_abort_flags = std::make_shared<OpenScenarioEngine::v1_2::EngineAbortFlags>(OpenScenarioEngine::v1_2::EngineAbortFlags::kNoAbort);
  auto entity_broker = std::make_shared<OpenScenarioEngine::v1_2::EntityBroker>(false);
  entity_broker->add("Ego");
  testing::OpenScenarioEngine::v1_2::FakeRootNode fake_root_node(env_, engine_abort_flags, entity_broker);

  auto by_entity_condition_under_test = detail::CreateByEntityConditionUnderTest(NET_ASAM_OPENSCENARIO::v1_2::TriggeringEntitiesRule::TriggeringEntitiesRuleEnum::ANY);

  fake_root_node.setChild(by_entity_condition_under_test);
  fake_root_node.distributeData();
  fake_root_node.onInit();

  EXPECT_THAT(fake_root_node.executeTick(), yase::NodeStatus::kSuccess);
  EXPECT_THAT(fake_root_node.executeTick(), yase::NodeStatus::kRunning);
  EXPECT_THAT(fake_root_node.executeTick(), yase::NodeStatus::kSuccess);
}
