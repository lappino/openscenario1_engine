#!/bin/bash

################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script executes the build and installs files to destination directory
################################################################################

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1

if hash nproc 2>/dev/null; then
  # calculation is kept for reference
  MAKE_JOB_COUNT=$(($(nproc)/1))
else
  # fallback, if nproc doesn't exist
  MAKE_JOB_COUNT=2
fi

# HOTFIX TILL RUNTIME DEPS ARE RESOLVED AUTOMATICALLY
if [[ "${OSTYPE}" = "msys" ]]; then
  cp $MYDIR/../../../../deps/direct_deploy/openscenario_api/lib/*.dll $MYDIR/../../../../build
  cp $MYDIR/../../../../deps/direct_deploy/yase/bin/*.dll $MYDIR/../../../../build
fi

make -j2 test ARGS="-j2 --output-on-failure"
